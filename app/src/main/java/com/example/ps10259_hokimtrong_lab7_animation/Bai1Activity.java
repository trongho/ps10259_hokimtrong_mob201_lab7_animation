package com.example.ps10259_hokimtrong_lab7_animation;

import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class Bai1Activity extends AppCompatActivity {
    ImageView ivBai1;
    Button btnRotation,btnMoving,btnZoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai1);

        initComponent();
        rotation();
        moving();
        zoom();
    }

    public void initComponent(){
        ivBai1=findViewById(R.id.ivBai1);
        btnRotation=findViewById(R.id.btnRotation);
        btnMoving=findViewById(R.id.btnMoving);
        btnZoom=findViewById(R.id.btnZoom);
    }

    public void rotation(){
        btnRotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float dest=360;
                if(ivBai1.getRotation()==360){
                    System.out.println(ivBai1.getAlpha());
                    dest=0;
                }

                ObjectAnimator animation = ObjectAnimator.ofFloat(ivBai1, "rotation", dest);
                animation.setDuration(5000);
                animation.start();
            }
        });
    }

    public void moving(){
        btnMoving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(ivBai1, "translationX", -300f,300f);
                animation.setDuration(5000);
                animation.start();
            }
        });
    }

    public void zoom(){
        btnZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation= AnimationUtils.loadAnimation(Bai1Activity.this,R.anim.zoom);
                ivBai1.startAnimation(animation);
            }
        });
    }

}
