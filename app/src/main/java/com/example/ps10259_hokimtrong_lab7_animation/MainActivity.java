package com.example.ps10259_hokimtrong_lab7_animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btnAnimation=findViewById(R.id.btnAnimation);

        ObjectAnimator animation1 = ObjectAnimator.ofFloat(btnAnimation, "translationX", 300f);
        animation1.setDuration(5000);
        //animationX.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(btnAnimation, "rotation", 800f);
        animation2.setDuration(5000);
        //animationY.start();



        AnimatorSet animatorSet=new AnimatorSet();
        //animatorSet.playSequentially(animation1,animation2);//tuần tự
        animatorSet.play(animation1).with(animation2);
        animatorSet.start();
    }
}
