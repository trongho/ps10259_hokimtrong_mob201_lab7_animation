package com.example.ps10259_hokimtrong_lab7_animation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Lab7Activity extends AppCompatActivity {
    Button btnBai1,btnBai2,btnBai3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab7);

        btnBai1=findViewById(R.id.btnBai1);
        btnBai2=findViewById(R.id.btnBai2);
        btnBai3=findViewById(R.id.btnBai3);

        btnBai1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Lab7Activity.this,Bai1Activity.class);
                startActivity(intent);
            }
        });

        btnBai2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Lab7Activity.this,Bai2Activity.class);
                startActivity(intent);
            }
        });

        btnBai3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Lab7Activity.this,Bai3Activity.class);
                startActivity(intent);
            }
        });
    }
}
