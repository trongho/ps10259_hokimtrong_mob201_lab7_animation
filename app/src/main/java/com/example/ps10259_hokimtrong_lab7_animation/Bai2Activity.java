package com.example.ps10259_hokimtrong_lab7_animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Bai2Activity extends AppCompatActivity {
    Button btnAll,btnDoraemon,btnNobita;
    ImageView ivAll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai2);

        initComponent();
        all();
        doraemon();
        nobita();
    }

    public void initComponent(){
        btnAll=findViewById(R.id.btnAll);
        btnDoraemon=findViewById(R.id.btnDraemon);
        btnNobita=findViewById(R.id.btnNobita);
        ivAll=findViewById(R.id.ivAll);
    }

    public void all(){
        btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage("All");
            }
        });
    }

    public void doraemon(){
        btnDoraemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage("Doraemon");
            }
        });
    }

    public void nobita(){
        btnNobita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage("Nobita");
            }
        });
    }

    private void showImage(String img){
        //hide image
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(ivAll, "translationX", 0f,500f);
        animation1.setDuration(2000);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(ivAll, "alpha", 1f,0f);
        animation2.setDuration(2000);

        //show image
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(ivAll, "translationX", 500f,0f);
        animation3.setDuration(2000);
        ObjectAnimator animation4 = ObjectAnimator.ofFloat(ivAll, "alpha", 0f,1f);
        animation4.setDuration(2000);

        //config slideshow
        AnimatorSet animatorSet=new AnimatorSet();
        animatorSet.play(animation3).with(animation4).after(animation1).after(animation2);
        animatorSet.start();
        final String imgName=img;

        //set listenner for determine when animation finished
        animation2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //change source image
                if(imgName=="All")
                    ivAll.setImageResource(R.drawable.all);
                if(imgName=="Nobita")
                    ivAll.setImageResource(R.drawable.nobita);
                if(imgName=="Doraemon")
                    ivAll.setImageResource(R.drawable.doraemon);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
}
