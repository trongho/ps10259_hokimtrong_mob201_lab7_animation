package com.example.ps10259_hokimtrong_lab7_animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;

public class Bai3Activity extends AppCompatActivity {
    ImageView ivBg,ivHour,ivMinute,ivSecond;
    Button btnRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai3);

        initComponent();
        run();
    }

    public void initComponent(){
        ivBg=findViewById(R.id.ivBg);
        ivHour=findViewById(R.id.ivHour);
        ivMinute=findViewById(R.id.ivMinute);
        ivSecond=findViewById(R.id.ivSecond);
        btnRun=findViewById(R.id.btnRun);
    }

    public void run(){
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator animationHour = ObjectAnimator.ofFloat(ivHour, "rotation", 360f);
                animationHour.setDuration(216000000);
                animationHour.setRepeatCount(Animation.INFINITE);
                ObjectAnimator animationMinute = ObjectAnimator.ofFloat(ivMinute, "rotation", 360f);
                animationMinute.setDuration(3600000);
                animationMinute.setRepeatCount(Animation.INFINITE);
                ObjectAnimator animationSecond = ObjectAnimator.ofFloat(ivSecond, "rotation", 360f);
                animationSecond.setDuration(60000);
                animationSecond.setRepeatCount(Animation.INFINITE);

                AnimatorSet animatorSet=new AnimatorSet();
                animatorSet.play(animationHour).with(animationMinute);
                animatorSet.play(animationMinute).with(animationSecond);
                animatorSet.start();



            }
        });
    }
}
